package com.jiusong.covid_19.location

import android.content.Context
import android.os.Looper
import com.google.android.gms.location.*

/**
 * A class wraps FusedLocationProviderClient for location updates.
 * Created by Jiusong.Gao on 3/22/20.
 */
class FusedLocationProvider(context: Context) {

    /**
     * Length of time in milliseconds to wait before the next location update is received
     */
    private val AUX_DELAY_MILLIS = 5 * 60000

    /**
     * Length of time in milliseconds to wait if a location is available sooner than AUX_DELAY_MILLIS you can get it
     * (i.e. another app is using the location services).
     */
    private val GPS_DELAY_MILLIS = 6000

    private val fusedLocationClient : FusedLocationProviderClient = FusedLocationProviderClient(context)
    private val locationRequest: LocationRequest = LocationRequest()
    private val locationCallback: LocationCallback
    private lateinit var locationListener: LocationListener

    init {

        locationRequest.interval = AUX_DELAY_MILLIS.toLong()
        locationRequest.fastestInterval = GPS_DELAY_MILLIS.toLong()
        locationRequest.maxWaitTime = (2 * AUX_DELAY_MILLIS).toLong()
        locationRequest.numUpdates = 1
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult?) {
                super.onLocationResult(result)
                if (result != null) {
                    locationListener.locationFound(result.lastLocation)
                }
            }

            override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
                super.onLocationAvailability(locationAvailability)
                if (locationAvailability != null && !locationAvailability.isLocationAvailable) {
                    locationListener.locationFailed()
                }
            }
        }
    }

    fun startUpdatingLocation(LocationListener: LocationListener) {
        this.locationListener = LocationListener
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
    }

    fun stopUpdatingLocation() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

}

