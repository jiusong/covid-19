package com.jiusong.covid_19.location

import android.content.Context
import android.location.Location
import android.os.Looper
import android.widget.Toast
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationServices.getFusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng


/**
 * Created by jiusong.gao on 3/22/20.
 */
object LocationHelper {
    private const val UPDATE_INTERVAL: Long = 10 * 1000 /* 10 secs */
    private const val FASTEST_INTERVAL: Long = 2000 /* 2 sec */
    private lateinit var mLocationRequest: LocationRequest
    private lateinit var context: Context

    fun with(context: Context) {
        this.context = context
        // Create the location request
        mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = UPDATE_INTERVAL
        mLocationRequest.fastestInterval = FASTEST_INTERVAL
    }

    fun startLocationUpdates(callback: LocationCallback) {
        // Create LocationSettingsRequest object using location request
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        val locationSettingsRequest = builder.build()
        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        val settingsClient = LocationServices.getSettingsClient(context)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        getFusedLocationProviderClient(context).requestLocationUpdates(
            mLocationRequest, object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    // do work here
                   callback.onLocationResult(locationResult)
                }
            },
            Looper.myLooper()
        )
    }

    fun stopLocationUpdates() {

    }

    fun onLocationChanged(location: Location) { // New location has now been determined
        val msg = "Updated Location: " +
                java.lang.Double.toString(location.getLatitude()) + "," +
                java.lang.Double.toString(location.getLongitude())
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        // You can now create a LatLng Object for use with maps
        val latLng = LatLng(location.getLatitude(), location.getLongitude())
    }
}