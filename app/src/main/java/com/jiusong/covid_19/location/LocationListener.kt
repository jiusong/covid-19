package com.jiusong.covid_19.location

import android.location.Location

/**
 * Created by jiusong.gao on 3/23/20.
 */
interface LocationListener {
    fun locationFound(loc: Location)
    fun locationFailed()
}