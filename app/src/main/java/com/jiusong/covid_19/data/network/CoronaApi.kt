package com.jiusong.covid_19.data.network

import retrofit2.http.GET

/**
 * Created by jiusong.gao on 3/21/20.
 */
interface CoronaApi {
    @GET("/v2/countries/")
    suspend fun getAllCountryCovidUpdate(): MutableList<CountryCovid>
}