package com.jiusong.covid_19.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.jiusong.covid_19.data.network.CountryCovid


/**
 * Created by jiusong.gao on 3/25/20.
 */
@Dao
interface CountryDao {

    @Insert(onConflict = REPLACE)
    fun save(countryCovid: CountryCovid)

    @Insert(onConflict = REPLACE)
    fun bulkInsert(countries: List<CountryCovid>)

    @Query("SELECT * FROM countryCovid WHERE country = :country")
    fun load(country: String): CountryCovid

    @Query("SELECT * FROM countryCovid")
    fun loadAllCountries(): List<CountryCovid>

    @Query("DELETE FROM countryCovid")
    fun deleteAllCountries()

}