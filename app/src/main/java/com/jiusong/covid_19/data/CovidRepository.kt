package com.jiusong.covid_19.data

import com.jiusong.covid_19.data.database.CountryDao
import com.jiusong.covid_19.data.network.ApiFactory
import com.jiusong.covid_19.data.network.CountryCovid

/**
 * Created by jiusong.gao on 3/21/20.
 */
class CovidRepository(private val countryDao: CountryDao) {

    private val service = ApiFactory.coronaApi

    suspend fun getCountryUpdates(): List<CountryCovid> {
        refreshCountryCovid()
        return countryDao.loadAllCountries()
    }

    private suspend fun refreshCountryCovid() {
        var countryUpdates = mutableListOf<CountryCovid>()
        try {
            countryUpdates = service.getAllCountryCovidUpdate()
            countryUpdates.sortWith(compareByDescending{it.cases})
            countryDao.bulkInsert(countryUpdates)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}