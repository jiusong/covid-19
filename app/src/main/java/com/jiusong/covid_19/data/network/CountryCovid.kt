package com.jiusong.covid_19.data.network

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by jiusong.gao on 3/21/20.
 */
@Entity
data class CountryCovid(@PrimaryKey val country: String,
                        val cases: Int,
                        val todayCases: String,
                        val deaths: String,
                        val todayDeaths: String,
                        val recovered: String,
                        val active: String,
                        val critical: String,
                        val casesPerOneMillion: String) {
}