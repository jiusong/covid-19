package com.jiusong.covid_19.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jiusong.covid_19.data.network.CountryCovid

/**
 * Created by jiusong.gao on 3/25/20.
 */
@Database(entities = [CountryCovid::class], version = 1)
abstract class CoronaDatabase : RoomDatabase() {
    abstract fun countryDao(): CountryDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: CoronaDatabase? = null

        fun getDatabase(context: Context): CoronaDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CoronaDatabase::class.java,
                    "countryCovid"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}