package com.jiusong.covid_19.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.jiusong.covid_19.R

/**
 * Created by jiusong.gao on 4/23/20.
 */
class SplashScreenActivity : AppCompatActivity() {
    private val DELAY_TIME = 1600L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        delayMainActivity()
    }

    private fun delayMainActivity() {
        val handler = Handler()
        handler.postDelayed({ launchMainActivity() }, DELAY_TIME)
    }

    private fun launchMainActivity() {
        val intent = Intent()
        intent.setClass(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}