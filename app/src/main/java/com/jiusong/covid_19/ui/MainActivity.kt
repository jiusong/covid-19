package com.jiusong.covid_19.ui

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jiusong.covid_19.R
import com.jiusong.covid_19.ui.nearby.NearbyFragment
import com.jiusong.covid_19.ui.home.HomeFragment
import com.jiusong.covid_19.ui.settings.SettingsFragment
import com.jiusong.covid_19.utils.PermissionUtil


class MainActivity : AppCompatActivity() {

    private lateinit var homeFragment: HomeFragment
    private lateinit var mapFragment: NearbyFragment
    private lateinit var settingsFragment: SettingsFragment
    private lateinit var active: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        setupBottomNaviListener(navView)

        loadFragments()
    }

    private fun setupBottomNaviListener(navView: BottomNavigationView) {
        navView.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.navigation_home -> {
                    supportFragmentManager.beginTransaction().hide(active).show(homeFragment).commit()
                    supportActionBar?.setTitle(R.string.title_home)
                    active = homeFragment
                }

                R.id.navigation_dashboard -> {
                    supportFragmentManager.beginTransaction().hide(active).show(mapFragment).commit()
                    supportActionBar?.setTitle(R.string.title_dashboard)
                    active = mapFragment
                    if (!PermissionUtil.hasLocationPermission(this)) {
                        PermissionUtil.requestLocationPermission(this)
                    } else {
                        mapFragment.startLocationUpdate()
                    }
                }

                R.id.navigation_notifications -> {
                    supportFragmentManager.beginTransaction().hide(active).show(settingsFragment).commit()
                    supportActionBar?.setTitle(R.string.title_notifications)
                    active = settingsFragment
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    private fun loadFragments() {
        settingsFragment = SettingsFragment()
        supportFragmentManager.beginTransaction().add(R.id.nav_host_fragment, settingsFragment, "Settings").hide(settingsFragment).commit()

        mapFragment = NearbyFragment()
        supportFragmentManager.beginTransaction().add(R.id.nav_host_fragment, mapFragment, "NearBy").hide(mapFragment).commit()

        homeFragment = HomeFragment()
        supportFragmentManager.beginTransaction().add(R.id.nav_host_fragment, homeFragment, "Home").commit()
        active = homeFragment
        supportActionBar?.setTitle(R.string.title_home)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            PermissionUtil.REQUEST_LOCATION_PERMISSION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    mapFragment.startLocationUpdate()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }
    }
}
