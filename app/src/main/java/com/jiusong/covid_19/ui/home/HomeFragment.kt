package com.jiusong.covid_19.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.jiusong.covid_19.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        binding = FragmentHomeBinding.inflate(layoutInflater)

        val viewManager = LinearLayoutManager(this.context)
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.layoutManager = viewManager

        showData()

        return binding.root
    }

    private fun showData() {
        homeViewModel.data.observe(viewLifecycleOwner, Observer {
            Log.i("TAG", "country count:${it.size}")
            binding.progressBar.visibility = View.GONE
            val adapter =  CountryCovidAdapter(it)
            binding.recyclerView.adapter = adapter
        })
    }
}
