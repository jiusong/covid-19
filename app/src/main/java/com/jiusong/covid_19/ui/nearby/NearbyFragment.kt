package com.jiusong.covid_19.ui.nearby

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.jiusong.covid_19.R
import com.jiusong.covid_19.databinding.FragmentNearbyBinding

class NearbyFragment : Fragment() {

    private lateinit var dashboardViewModel: NearbyViewModel
    private lateinit var binding: FragmentNearbyBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
                ViewModelProviders.of(this).get(NearbyViewModel::class.java)
        binding = FragmentNearbyBinding.inflate(inflater)
        initMapWebView()
        return binding.root
    }

    private fun initMapWebView() {
        binding.mapView.settings.javaScriptEnabled = true
        binding.mapView.webViewClient = MyWebViewClient()
    }

    fun startLocationUpdate() {
        val liveData = dashboardViewModel.getLocationData()
        liveData.observeOnce(this, Observer {
            val url = getString(R.string.covid_map_url, it.latitude.toString(), it.longitude.toString())
            Log.i("TAG", "map url: $url")
            binding.mapView.loadUrl(url)
        })
    }

    private fun <T> LiveData<T>.observeOnce(lifecycleOwner: LifecycleOwner, observer: Observer<T>) {
        observe(lifecycleOwner, object : Observer<T> {
            override fun onChanged(t: T?) {
                observer.onChanged(t)
                removeObserver(this)
            }
        })
    }
    inner class MyWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return false
        }

        override fun onPageFinished(view: WebView, url: String) {
            binding.progressBar.visibility = View.GONE
        }
    }
}
