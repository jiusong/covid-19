package com.jiusong.covid_19.ui.nearby

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jiusong.covid_19.location.LocationLiveData

class NearbyViewModel(application: Application) : AndroidViewModel(application) {

    private val locationData = LocationLiveData(application)

    private val _url = MutableLiveData<String>().apply {
        value = "https://www.google.com/maps/d/viewer?mid=1yCPR-ukAgE55sROnmBUFmtLN6riVLTu3&hl=en&fbclid=IwAR0VZ0net0x6_yL_cGL2xqSuxoElWY1u8YC3grOyjveMsRwdvDWaDCo71Ck&ll=36.21765521518956%2C-117.52772348332661&z=4"
    }

    val url: LiveData<String> = _url

    fun getLocationData() = locationData
}