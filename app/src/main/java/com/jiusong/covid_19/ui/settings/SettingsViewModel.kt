package com.jiusong.covid_19.ui.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jiusong.covid_19.BuildConfig

class SettingsViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = BuildConfig.VERSION_NAME
    }
    val text: LiveData<String> = _text
}