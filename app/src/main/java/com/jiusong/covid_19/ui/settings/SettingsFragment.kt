package com.jiusong.covid_19.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.jiusong.covid_19.BuildConfig
import com.jiusong.covid_19.R
import com.jiusong.covid_19.databinding.FragmentSettingsBinding
import de.cketti.mailto.EmailIntentBuilder

class SettingsFragment : Fragment() {

    private lateinit var settingsViewModel: SettingsViewModel
    private lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        settingsViewModel = ViewModelProviders.of(this).get(SettingsViewModel::class.java)
        binding = FragmentSettingsBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        settingsViewModel.text.observe(viewLifecycleOwner, Observer {
            binding.version.text = getString(R.string.version, it)
        })

        binding.contactUs.setOnClickListener { contactUs() }
    }

    /**
     * Contact us by email.
     */
    private fun contactUs() {
        val emailIntentBuilder = EmailIntentBuilder.from(requireActivity().baseContext)
        emailIntentBuilder.to(getString(R.string.contact_email))
            .subject(getString(R.string.contact_us_subject))
            .body(getEmailMessage())
            .start()
    }

    private fun getEmailMessage(): String {
        val appName = getString(R.string.app_name)
        val versionName: String = BuildConfig.VERSION_NAME
        return "$appName Android App v $versionName\n"
    }
}
