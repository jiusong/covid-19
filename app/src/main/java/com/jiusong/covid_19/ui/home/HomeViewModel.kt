package com.jiusong.covid_19.ui.home

import android.app.Application
import androidx.lifecycle.*
import com.jiusong.covid_19.data.CovidRepository
import com.jiusong.covid_19.data.database.CoronaDatabase
import com.jiusong.covid_19.data.network.CountryCovid
import kotlinx.coroutines.Dispatchers

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val database =  CoronaDatabase.getDatabase(application)
    private val repository = CovidRepository(database.countryDao())

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    val data : LiveData<List<CountryCovid>> = liveData(Dispatchers.IO) {
        val retrievedData = repository.getCountryUpdates()
        emit(retrievedData)
    }
}