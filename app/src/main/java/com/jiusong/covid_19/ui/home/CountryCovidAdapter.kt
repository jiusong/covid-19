package com.jiusong.covid_19.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jiusong.covid_19.R
import com.jiusong.covid_19.data.network.CountryCovid

/**
 * Created by jiusong.gao on 3/22/20.
 */
class CountryCovidAdapter(private val myDataset: List<CountryCovid>) : RecyclerView.Adapter<CountryCovidAdapter.MyViewHolder>() {

    class MyViewHolder(root: View) :
        RecyclerView.ViewHolder(root) {

        private var totalConfirmed: TextView
        private var newConfirmed: TextView
        private var totalRecovered: TextView
        private var totalDeath: TextView
        private var newDeath: TextView
        private var country: TextView = root.findViewById(R.id.country)

        init {
            totalConfirmed = root.findViewById(R.id.total_confirmed)
            totalRecovered = root.findViewById(R.id.total_recovered)
            newConfirmed = root.findViewById(R.id.new_confirmed)
            totalDeath = root.findViewById(R.id.total_death)
            newDeath = root.findViewById(R.id.new_death)
        }

        fun bind(countryCovid: CountryCovid) {
            country.text = countryCovid.country
            totalConfirmed.text = countryCovid.cases.toString()
            newConfirmed.text = "New: +" + countryCovid.todayCases
            totalRecovered.text = countryCovid.recovered
            totalDeath.text = countryCovid.deaths
            newDeath.text = "New: +" + countryCovid.todayDeaths
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.country_covid, parent, false)
        return MyViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val countryCovid = myDataset[position]
        holder.bind(countryCovid)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}